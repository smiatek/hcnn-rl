import matplotlib.pyplot as plt
from matplotlib import colors as map_colors
import numpy as np
from pathlib import Path, PureWindowsPath
import os
from IPython.display import display


class VisualizeModel:
    def __init__(self, observation_names, loss_only=False, use_jupyter=False):
        self.use_jupyter = use_jupyter
        self.loss_only = loss_only
        if self.loss_only:
            self.fig, self.loss_ax = plt.subplots(figsize=(15, 10), nrows=1)
        else:
            self.fig, self.__axes = plt.subplots(figsize=(15, 15), nrows=len(observation_names) + 1)
            self.loss_ax = self.__axes[0]
            self.axes = {}
            self.observation_names = observation_names
            for i, names in enumerate(self.observation_names):
                self.axes[names] = self.__axes[i + 1]

        self.colors = list(dict(map_colors.BASE_COLORS, **map_colors.CSS4_COLORS).keys())
        self.save_destination = 'plots'
        if not os.path.isdir(self.save_destination):
            os.mkdir(self.save_destination)

    def plot(self, epoch, loss_array, actual_np_with_action=None, val_loss_array=None, test_np=None, file_name=None):
        self.plt_loss(epoch, loss_array, val_loss_array)
        if not self.loss_only:
            self.plt_data(actual_np_with_action, test_np)
        if file_name is None:
            self.update(epoch)
        else:
            self.update(f'{epoch}_{file_name}')

    def plt_loss(self, epoch, loss_array, val_loss_array):
        x_values = np.arange(1, len(loss_array) + 1)
        y_values = np.array(loss_array)
        self.loss_ax.plot(x_values, y_values, 'green', label='training loss')
        if val_loss_array is not None:
            val_x_values = np.arange(1, len(val_loss_array) + 1)
            val_y_values = np.array(val_loss_array)
            self.loss_ax.plot(val_x_values, val_y_values, 'red', label='validation loss')

        self.loss_ax.set_xlabel('epoch')
        self.loss_ax.set_ylabel('loss')
        self.loss_ax.set_title(f'Training loss on epoch {epoch}: {loss_array[-1]}')
        self.loss_ax.legend(loc='upper right')

    def plt_data(self, actual_np, test_np):
        for i, name in enumerate(self.observation_names):
            self.axes[name].plot(actual_np.take(i + 1, axis=1), '--', label=f'actual {name}', color=self.colors[i],
                                 alpha=0.3)
            if test_np is not None:
                self.axes[name].plot(test_np.take(i, axis=1), label=f'scaled {name}', color=self.colors[i], alpha=0.5)
            self.axes[name].set_title(f'prediction of {name}')
            legend_pred = self.axes[name].legend(frameon=True, loc='upper right')
            legend_pred.get_frame().set_color('white')

    def update(self, file_name):
        if self.use_jupyter:
            display(self.fig)
        else:
            self.fig.show()
        self.save_figure(file_name)
        self.clear_all_axes()

    def save_figure(self, file_name):
        file_to_save = Path(PureWindowsPath(f"{self.save_destination}\\{file_name}.png"))
        self.fig.savefig(file_to_save, format='png')

    def clear_all_axes(self):
        self.loss_ax.clear()
        if not self.loss_only:
            for ax in self.axes.values():
                ax.clear()


class VisualizeRL:
    def __init__(self, use_jupyter=False):
        self.fig, self.reward_ax = plt.subplots(figsize=(10, 8), nrows=1)
        self.use_jupyter = use_jupyter
        self.save_destination = Path('plots/RL_plots')
        if not self.save_destination.exists():
            os.mkdir(self.save_destination)
        self.colors = list(dict(map_colors.BASE_COLORS, **map_colors.CSS4_COLORS).keys())

    def all_plot(self, error_rate, complete, predictions_array, color_array, queue_sizes, last_value, file_name,
                 random=None):
        self.reward_ax.plot(complete, '#3CB372', label='Complete')
        for p, loss_pred in enumerate(predictions_array):
            plt.plot(loss_pred, color_array[p], label=f'Prediction queue {queue_sizes[p]}')
        self.reward_ax.plot(last_value, '#ff0000', label='Last Value')
        if random is not None:
            self.reward_ax.plot(random, '#ffa500', label="Random Values")

        self.reward_ax.set_title(f'Mean Q-learning reward over time, error rate: {error_rate}, complete Loss ')
        self.reward_ax.set_xlabel('q-learning epochs')
        self.reward_ax.set_ylabel('mean q-learning reward')
        self.reward_ax.legend(loc='best')

        self.update(file_name, final_er=error_rate)

    def auto_label(self, rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            self.reward_ax.annotate('{}'.format(height),
                                    xy=(rect.get_x() + rect.get_width() / 2, height),
                                    xytext=(0, 3),  # 3 points vertical offset
                                    textcoords="offset points",
                                    ha='center', va='bottom')

    def bar_chart_plot(self, complete, predicted, last_value, random, queue_sizes, error_rate, file_name):
        x = np.arange(len(queue_sizes))  # the label locations
        bar_width = 0.35  # the width of the bars
        rects1 = self.reward_ax.bar(x - bar_width / 2, predicted, bar_width, color='#1db4a5', label='Prediction')
        rects2 = self.reward_ax.bar(x + bar_width / 2, last_value, bar_width, label='Last Value')
        self.reward_ax.set_ylabel('Mean reward in last step')
        self.reward_ax.set_xlabel('Queue length')
        self.reward_ax.set_title(f'Reward by queue_length and lossy type, errorRate {error_rate}')
        self.reward_ax.set_xticks(x)
        self.reward_ax.set_xticklabels(queue_sizes)
        self.reward_ax.legend(loc='lower right')

        self.auto_label(rects1)
        self.auto_label(rects2)

        self.update(file_name, final_er=error_rate)

    def reward_quotient_plot(self, error_rates, quotient_to_rule_all_quotients, queue_sizes, file_name):
        self.reward_ax.set_title(f'prediction/last_value quotient relative to queue length')
        for i, error_rate in enumerate(error_rates):
            plt.plot(quotient_to_rule_all_quotients[i][:], label=f'error rate {error_rate}')
        self.reward_ax.set_xlabel('queue_sizes')
        self.reward_ax.set_ylabel('pred/last_value quotient')
        self.reward_ax.set_xticks(range(len(queue_sizes)))
        self.reward_ax.set_xticklabels(queue_sizes)
        self.reward_ax.legend(loc='upper right')

        self.update(file_name)

    def single_plot(self, reward, file_name):
        self.reward_ax.plot(reward)
        self.reward_ax.set_xlabel('epoch')
        self.reward_ax.set_ylabel('reward')
        self.reward_ax.set_title('Reward per epoch')

        self.update(file_name)

    def update(self, file_name, final_er=None):
        if self.use_jupyter:
            display(self.fig)
        else:
            self.fig.show()
        self.save_figure(file_name, final_er)
        self.clear_all_axes()

    def save_figure(self, file_name, final_er):
        if final_er is not None:
            file_to_save = Path(
                PureWindowsPath(f"{self.save_destination}\\source_files\\final{final_er}\\{file_name}.png"))
        else:
            file_to_save = Path(PureWindowsPath(f"{self.save_destination}\\{file_name}.png"))
        self.fig.savefig(file_to_save, format='png')

    def clear_all_axes(self):
        self.reward_ax.clear()
