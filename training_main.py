import numpy as np
import torch
from tqdm import tqdm
from hcnn import HCNN
import os
import sys
from data_control import MyDataset
import torch.utils.data
from visualisation import VisualizeModel
from pathlib import Path
import itertools

class Trainer:
    def __init__(self, hidden_dim=50, data_set=None, learning_rate=0.08, sequence_length=5, epochs=100):
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        if data_set is not None:
            self.dataset = data_set
        else:
            self.dataset = MyDataset(data_length=10000)
        self.training_data, self.valid_data = torch.utils.data.random_split(self.dataset,
                                                                            [int(len(self.dataset) * 0.7),
                                                                             int(len(self.dataset) * 0.3)])
        self.observation_names = ['cart_position', 'cart_velocity', 'pole_angle', 'pole_velocity']
        self.input_size = self.dataset.shape[1] - 1  # only observations without action
        assert len(self.observation_names) == self.input_size, "observations should have the same size as the read data"

        # set hyper parameters
        self.hidden_dim = hidden_dim  # Number of Neurons in hidden layer
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.seq_len = sequence_length

        self.visualizer = VisualizeModel(self.observation_names, loss_only=True)

        self.model = HCNN(input_size=self.input_size, hidden_dim=self.hidden_dim, std=self.dataset.std,
                          mean=self.dataset.mean).float()

    def __str__(self):
        return f'{len(self.dataset)}d_{self.hidden_dim}h_{self.learning_rate}lr_{self.seq_len}seq'

    def train(self, verbose=True):
        """
        Trains the model.
        :param verbose: Enables printing between epochs
        :return: mean training loss over all the epochs
                 mean validation loss over all the epochs
        """
        optimizer = torch.optim.SGD(self.model.parameters(), lr=self.learning_rate)
        loss_function = torch.nn.MSELoss()

        epoch_losses = []
        val_epoch_loss = []
        mean_epoch_losses = []
        val_mean_epoch_losses = []
        for epoch_nr in tqdm(range(self.epochs), desc='Training'):
            for data_nr, training_d in enumerate(self.training_data):
                if len(training_d) <= self.seq_len:
                    continue
                data_loss = self.bptt(training_d, optimizer, loss_function)
                epoch_losses.append(data_loss)
            mean_epoch_losses.append(np.asarray(epoch_losses).mean())
            val_epoch_loss, last_data = self.validate(val_epoch_loss, loss_function)
            val_mean_epoch_losses.append(np.asarray(val_epoch_loss).mean())
            if verbose and epoch_nr % 5 == 0:
                self.visualizer.plot(f'{epoch_nr + 1} {str(self)}', mean_epoch_losses, last_data.detach().numpy(),
                                     val_loss_array=val_mean_epoch_losses)
        self.save_model()
        self.visualizer.plot(self.epochs, mean_epoch_losses, val_loss_array=val_mean_epoch_losses, file_name=str(self))
        return np.asarray(mean_epoch_losses), np.asarray(val_mean_epoch_losses)

    def bptt(self, training_data, opt, loss_function):
        """
        Back-propagation through time
        Used while training the method on a training_data
        :param training_data:
        :param opt: optimization function which used to update the model parameters.
        :param loss_function: Loss function used to calculate the loss (e.g. Mean Squared Error)
        :return:
        """

        input_sequence, ground_truth = self.sequence_separator(training_data)
        self.model.zero_grad()
        y, s = self.model.forward(input_sequence)
        loss = loss_function(y, ground_truth[1:].float())
        loss.backward()
        opt.step()  # update model parameters

        return loss.item()

    def sequence_separator(self, whole_sequence):
        """
        Separates a sequence into a subsequence with the length self.seq_len
        :param whole_sequence: sequence to separate
        :return: subsequence
                 next value after the subsequence
        """
        random_start_index = np.random.randint(len(whole_sequence) - self.seq_len)
        data_sequence = whole_sequence[random_start_index:][:self.seq_len]
        ground_truth = whole_sequence[random_start_index + self.seq_len]
        return torch.tensor(data_sequence), torch.tensor(ground_truth)

    def save_model(self, hyper_parameters=False):
        """
        Saves the model state dict.
        :param hyper_parameters: If true, the hyper-parameters of the trainer
                                 are used for the naming of the saving state
        """
        if hyper_parameters:
            file_name = f'state_{str(self)}.pt'
        else:
            file_name = 'state.pt'
        file_to_save = Path("saves") / file_name
        model_state_dict = self.model.state_dict()
        torch.save(model_state_dict, file_to_save)

    def validate(self, val_loss, loss_function):
        """
        Validate the model on the validation data.
        :param val_loss: validation loss array until this time.
        :param loss_function: Loss function used to calculate the loss (e.g. Mean Squared Error)
        :return: - next validation loss array
                 - the last input sequence of the validation (can be used for other plotting the sequence)
        """
        for val_data in self.valid_data:
            if len(val_data) <= self.seq_len:
                continue
            with torch.no_grad():
                input_sequence, ground_truth = self.sequence_separator(val_data)
                y, s = self.model.forward(input_sequence)
                loss = loss_function(y, ground_truth[1:].float())
                val_loss.append(loss.item())
        return val_loss, input_sequence


def hyper_parameter_tuning():
    """
    Add or delete elements in the arrays: learning_rate, seq_len, hidden, epochs
    This arrays will be combined with each other to every possible combination.
    For all the combined arrays a new Trainer will be initialized (with the same data set), trained and saved.
    """
    save_destination = 'hyperparameter_tuning'
    if not os.path.isdir(save_destination):
        os.mkdir(Path(save_destination))

    # Notes of the hyperparameter tuning
    # Best values 0.08 (and 0.09)
    learning_rate = [0.08]
    # 2 -> 0.090 loss
    # 5 -> 0.108 loss
    # 6 -> not converged by 50 epochs and 10000 data
    # 7 -> 0.124
    # 8 -> 0.132
    seq_len = [1, 7, 15, 20]
    # No big difference between hidden sizes --> 50
    hidden = [50]
    epochs = [100]
    # Best: Seq 5, LR 0.08, hidden 50
    # combination seq 6 and lr 0.09 are also good

    training_means = []
    val_means = []

    whole_dataset = Trainer().dataset  # Load data set once and use it for all runs
    for lr, seq, h, e in itertools.product(*[learning_rate, seq_len, hidden, epochs]):
        print(f'\n-----------------------------Start training------------------------------\n'
              f'{lr} learning rate, {seq} sequence length, {h} hidden dimension, for {e} epochs')
        trainer = Trainer(h, whole_dataset, lr, seq, e)
        training_loss_mean, validation_loss_mean = trainer.train()
        print(f'Best loss {np.min(training_loss_mean)} on epoch {training_loss_mean.argmin()} \n'
              f'Best validation {np.min(validation_loss_mean)} on epoch {validation_loss_mean.argmin()}')
        training_means.append(training_loss_mean)
        val_means.append(validation_loss_mean)
        trainer.save_model(hyper_parameters=True)

    training_means = np.asarray(training_means)
    val_means = np.asarray(val_means)
    np.save(Path(f'{save_destination}\\training_means.npy'), training_means)
    np.save(Path(f'{save_destination}\\val_means.npy'), val_means)


if __name__ == '__main__':
    prnt = True  # Turn off every print to accelerate training if False
    original = True
    if not prnt:
        sys.stdout = open(os.devnull, 'w')
    if original:
        Trainer().train()
    else:
        hyper_parameter_tuning()

    print("Simulation Finish")
