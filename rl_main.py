import itertools

import gym
import torch
import numpy as np
import math
import sys
from tqdm import tqdm
from pathlib import Path
import queue

from data_control import RlData, RlDataSaver
from hcnn import HCNN
from visualisation import VisualizeRL

class Learner:
    def __init__(self, queue_length, error_rate, lossy_type='', alpha=0.1, gamma=0.99):
        """
        :param queue_length: Length of the Queue since, when a predictor should be used
        :param error_rate: How likely the environment state (observation) get lost
        :param alpha: Reinforcement Learning Hyper parameter, learning rate
        :param gamma: Reinforcement Learning Hyper parameter, how strongly future rewards are weighted
        :param lossy_type: 'prediction', 'random', 'last_value'
        """
        self.env = gym.make('CartPole-v0')

        self.queue_length = queue_length #queue_length must be >0 if prediction is used
        self.error_rate = error_rate
        self.lossy_type = lossy_type
        self.lossy = True if lossy_type else False
        if queue_length != 0:
            self.predictor = self.load_predictor_model(self.queue_length)

        self.epochs = 2000
        self.q_table = np.zeros((2, 2, 8, 4, 2))
        self.alpha = alpha
        self.gamma = gamma
        self.rewards = []

        self.visualizer = VisualizeRL()

    @staticmethod
    def load_predictor_model(queue_length):
        predictor = HCNN(input_size=4, hidden_dim=50)
        try:
            predictor.load_state_dict(
                torch.load(Path('saves') / f'state_10000d_50h_0.08lr_{queue_length}seq_100epochs.pt'))
        except Exception as e:
            raise FileNotFoundError(f'{e} \n No such State created. Use training_main to create model state dict')
        predictor.eval()
        return predictor

    @staticmethod
    def to_discrete_states(observation):
        """
        Compute discrete states to reduce range of possible states
        :param observation:
        :return:
        """
        interval = [0 for _ in range(len(observation))]
        max_range = [2, 3, 0.42, 3]
        buckets = [2, 2, 8, 4]

        for o, o_data in enumerate(observation):
            inter = int(math.floor((o_data + max_range[o]) / (2 * max_range[o] / buckets[o])))
            if inter >= buckets[o]:
                interval[o] = buckets[o] - 1
            elif inter < 0:
                interval[o] = 0
            else:
                interval[o] = inter
        return interval

    def get_action(self, observation, t):
        """
        Select action for q_learning, either exploration or exploitation
        :param observation:
        :param t: episode length
        :return: action choose by episode-greedy
        """
        if np.random.random() < max(0.05, min(0.5, 1.0 - math.log10((t + 1) / 150.))):
            return self.env.action_space.sample()
        interval = self.to_discrete_states(observation)

        if self.q_table[tuple(interval)][0] >= self.q_table[tuple(interval)][1]:
            return 0
        else:
            return 1

    def update_q(self, actual_observation, reward, action, prev_observation):
        """
        Update q-table according to formula using learning rate self.alpha and discount favor self.gamma
        :param actual_observation:
        :param reward:
        :param action:
        :param prev_observation:
        :return:
        """
        interval = self.to_discrete_states(actual_observation)
        q_next = max(self.q_table[tuple(interval)][0], self.q_table[tuple(interval)][1])
        ini_interval = self.to_discrete_states(prev_observation)
        self.q_table[tuple(ini_interval)][action] += self.alpha * (
                reward + self.gamma * q_next - self.q_table[tuple(ini_interval)][action])

    def learn(self):
        """
        Train RL agent using q-learning
        If self.lossy is set to True, self.error_rate percent of data will be made lossy and substituted
        using the method specified in self.lossy_type (prediction, last_value or random)
        :return:
        """
        self.rewards = []
        rewards_sum = 0
        print(f'Start Learning with parameters: Queue length {self.queue_length}, Error Rate: {self.error_rate}, '
              f'Lossy Type: {self.lossy_type}')
        for i_episode in tqdm(range(1, self.epochs + 1)):
            prev_observation = self.env.reset()
            t = 0
            if self.queue_length != 0:
                history = queue.Queue(self.queue_length)
            done = False
            while not done:
                # self.env.render()
                action = self.get_action(prev_observation, i_episode)
                actual_observation, reward, done, info = self.env.step(action)
                # possible lossy observations
                if self.queue_length != 0:
                    if self.lossy and history.full() and np.random.uniform() < self.error_rate:
                        # check that enough backlog exists to use predictor (and all other methods) on that basis
                        actual_observation = self.lossy_data(prev_observation, self.lossy_type, history)
                else:
                    if self.lossy and np.random.uniform() < self.error_rate:
                        # check that enough backlog exists to use predictor (and all other methods) on that basis
                        actual_observation = self.lossy_data(prev_observation, self.lossy_type)

                self.update_q(actual_observation, reward, action, prev_observation)
                prev_observation = actual_observation
                # manage history
                action = np.asarray(action)
                if self.queue_length != 0:
                    if history.full():
                        history.get()  # pop if queue is full to make room
                    history.put(self.generate_element_for_queue(action, prev_observation))

                # calculate rewards
                t += reward
            # print(f"Episode finished after {t+1} timesteps")
            rewards_sum += (t + 1)
            self.rewards.append(rewards_sum / i_episode)

        fig_file_name = f'{self.lossy_type}_errorRate{self.error_rate}_completeloss_{self.queue_length}'
        self.visualizer.single_plot(self.rewards, fig_file_name)
        self.save_reward_array()

    def lossy_data(self, prev_observation, lossy_type, history=None):
        """
        Returns estimated next state using method specified by lossy_type
        :param history: queue of last self.queue_size states
        :param prev_observation: observation of previous step
        :param lossy_type: type of estimation, can be 'prediction'(using HCNN),
                            'last_state'(simply duplicates last seen state),
                            'random'(random values within the possible cartpole value ranges)
        :return:
        """
        if lossy_type == 'prediction':
            prediction, hidden_state = self.predictor.forward(torch.tensor((list(history.queue))))
            return prediction.detach().numpy()
        if lossy_type == 'last_value':
            return prev_observation
        else:  # lossy_type == 'random':
            max_size = sys.maxsize
            return [np.random.uniform(low=-2.4, high=2.4),
                    np.random.uniform(low=-max_size, high=max_size),
                    np.random.uniform(low=-41.8, high=41.8),
                    np.random.uniform(low=-max_size, high=max_size)]

    @staticmethod
    def generate_element_for_queue(action, observation):
        """
        Generate action-observation tuple for use in HCNN input sequence
        :param action:
        :param observation:
        :return:
        """
        action = np.asarray(action)
        row_tuple = np.append(action, observation)
        return row_tuple

    def save_reward_array(self):
        rld = RlDataSaver(self.error_rate)
        if self.lossy:
            file_name = f'rewards_LR{self.alpha}_epochs{self.epochs}_Lossy{self.lossy}_{self.lossy_type}_' \
                        f'errorRate{self.error_rate}_completeloss_{self.queue_length}.npy'
            rld.save_rewards(file_name, self.rewards)
        else:
            file_name = f'rewards_LR{self.alpha}_epochs{self.epochs}_Lossy{self.lossy}.npy'
            rld.save_rewards(file_name, self.rewards)


class Comparer:
    def __init__(self, alpha=0.1, epochs=2000):
        self.alpha = alpha
        self.epochs = epochs
        self.visualizer = VisualizeRL()


class ErrorRateComparer(Comparer):
    def __init__(self, queue_sizes, error_rate, alpha=0.1, epochs=2000):
        super().__init__(alpha, epochs)
        self.queue_sizes = queue_sizes
        self.error_rate = error_rate

        self.rl_data = RlData(self.error_rate, self.epochs, self.alpha)

    def compare_on_error_rate(self, error_rate):
        """
        Compare performance of RL Agent according to which kind of (potentially lossy) data it was trained on
        :return:
        """
        complete, lossy_predictions, _, lossy_random = self.rl_data.load_reward_arrays(self.queue_sizes)
        lossy_last = self.rl_data.load_data_with_last_value(queue_size='1')
        color_array = ['#62e5d8', '#1db4a5', '#9d9dff', '#4e4eff', '#0000ff', '#0000b1', '#000062']
        fig_file_name = f'compare_rewards_LR{self.alpha}_epochs{self.epochs}_errorRate{error_rate}'
        self.visualizer.all_plot(error_rate, complete, lossy_predictions, color_array, queue_sizes_array,
                                 lossy_last, fig_file_name, lossy_random)

    def compare_bar_chart(self, error_rate):
        """
        Compare performance of RL Agent according to which kind of (potentially lossy) data it was trained on
        Looks specifically at the difference in rewards when using either last_value or prediction data to train
        :return:
        """
        complete_last, predicted_last, last_values_last, lossy_random_last = self.rl_data.load_last_reward_arrays(
            self.queue_sizes)

        file_to_save = Path(
            f"compare_rewards_LR{self.alpha}_epochs{self.epochs}_errorRate{error_rate}_barplot.png")
        self.visualizer.bar_chart_plot(complete_last, predicted_last, last_values_last, lossy_random_last,
                                       self.queue_sizes, error_rate, file_to_save)


class QueueSizeQuotientComparer(Comparer):
    def __init__(self, queue_sizes, error_rates, constant_last_value=False, alpha=0.1, epochs=2000):
        super().__init__(alpha, epochs)
        self.constant_last_value = constant_last_value  # if False, last_values are loaded according to queue size
        self.queue_sizes = queue_sizes
        self.error_rates = error_rates

    def the_plot_to_rule_all_plots(self):
        """
        Computes a quotient of last_value rewards and corresponding prediction rewards for each queue_size
        and each error_rate.
        All quotients are mapped together to showcase the dynamics of the entire lossy_data training system
        :return:
        """

        quotient_to_rule_all_quotients = []

        # iterate over all error rates
        for error_rate in self.error_rates:
            rl_data = RlData(error_rate, self.epochs, self.alpha)
            rl_data.set_error_rate(error_rate)
            complete_last, predicted_values_lossy_last, last_values_lossy_last, _ = \
                rl_data.load_last_reward_arrays(self.queue_sizes)

            quotients = []
            # compute quotient of both rewards for each queue_size
            if self.constant_last_value is False:
                # loads last_value independent from queue size, thus constant scalar
                for i, queue_size in enumerate(self.queue_sizes):
                    quotients.append(predicted_values_lossy_last[i] / last_values_lossy_last[i])
            else:
                # loads last_value of respective queue size
                constant_last_value_lossy = int(rl_data.load_data_with_last_value()[-1])
                for i, queue_size in enumerate(self.queue_sizes):
                    quotients.append(predicted_values_lossy_last[i] / constant_last_value_lossy)

            # for each error_rate, append list of quotients to matrix
            quotient_to_rule_all_quotients.append(quotients)

        file_to_save = f'compare_rewards_LR{self.alpha}_epochs{self.epochs}_plot_to_rule_all_plots'
        self.visualizer.reward_quotient_plot(self.error_rates, quotient_to_rule_all_quotients, self.queue_sizes,
                                             file_to_save)


if __name__ == '__main__':
    learn_all = True
    compare_charts = False
    compare_relations = False
    queue_sizes_array = [1, 2, 5, 7, 10, 15, 20]
    error_rates_array = [0.05, 0.15, 0.3]  # [0.1, 0.2, 0.5, 0.7, 0.9]
    if learn_all:
        lossy = ['prediction', 'last_value', 'random']
        #generate files for all lossy types, for all error rates and all queue sizes
        #for q_size, er, ty in itertools.product(*[queue_sizes_array, error_rates_array, lossy]):
            #learner = Learner(q_size, er, lossy_type=ty)
            #learner.learn()
        #for lossy types generate files without usage of queue (~ queue size 0)
        for er in error_rates_array:
            Learner(0, er).learn()  # complete data
            Learner(0, er, lossy_type='last_value').learn() #last_value without queue for each error rate
            Learner(0, er, lossy_type='random').learn() #random without queue for each error rate

    else:
        if compare_charts:
            for er in error_rates_array:
                comparer = ErrorRateComparer(queue_sizes_array, er)
                comparer.compare_on_error_rate(er)
        if compare_relations:
            comparer = QueueSizeQuotientComparer(queue_sizes_array, error_rates_array)
            comparer.the_plot_to_rule_all_plots()
