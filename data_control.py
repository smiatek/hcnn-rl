import gym
import numpy as np
import os
from pathlib import Path  # used for Mac and Windows crossover
from tqdm import tqdm

import torch.utils.data


class MyDataset(torch.utils.data.Dataset):
    def __init__(self, data_length):
        self.data = []
        # all_data used to calculate the mean, std, and shape of the whole data set
        all_data = EnvironmentLoader(Path('stop_if_done')).load(f'env_data_0_with_reset.npy', root=True)
        self.data.append(all_data)  # all data initialized with the first data
        for i in tqdm(range(1, data_length), desc='Loading data'):
            one_d = EnvironmentLoader(Path('stop_if_done')).load(f'env_data_{i}_with_reset.npy', root=True)
            all_data = np.concatenate([all_data, one_d])
            self.data.append(one_d)
        self.std = torch.tensor(np.std(all_data, axis=0)[1:])  # leave out action column
        self.mean = torch.tensor(np.mean(all_data, axis=0)[1:])  # leave out action column
        self.shape = all_data.shape

    def __len__(self):
        return len(self.data)

    def __getitem__(self, item):
        return self.data[item]


class EnvironmentLoader:
    def __init__(self, directory_location):
        self.directory = directory_location
        if not os.path.isdir(self.directory):
            os.mkdir(self.directory)

    def save(self, index, array_like, every=False):
        """
        Saves data sample.
        :param index: of the data
        :param array_like: only array_like objects can be saved into a .npy file
        :param every: this gives an opportunity to save all the array_likes objects created in one run to a separate
                        folder ('all')
        :return:
        """
        a = Path('/all') if every else ''
        np.save(Path(f'{self.directory}{a}') / f'env_data_{index}_with_reset.npy', array_like)

    def load(self, file_name, root=False):
        """
        Load data sample
        :param file_name: need to be a .npy File
        :param root: if this is set, the root directory is used which was given in the constructor
        :return: file content as a numpy array
        """
        if root:
            file_name = Path(f'{self.directory}') / f'{file_name}'
        return np.load(file_name, allow_pickle=True)

    def create_data(self, environment, start=0, stop=1000):
        """
        Creates Training Samples of a given environment.
        If you already created a training sample, there is a way to add more dataset without overriding the old ones.
        Only add your last index number (start) and give the stopping index (stop).
        :param environment: gym environment (e.g. 'CarPole-v0')
        :param start: Starting index of the data sample
        :param stop: Stopping index of the data sample
        :return:
        """
        all_data = []
        for i in tqdm(range(start, stop), desc='Creating Training data'):
            data = []
            environment.reset()
            for _ in range(1000):
                action = environment.action_space.sample()
                observation, reward, done, info = environment.step(action)
                # self.environment.render()
                # observation range = cart_position(-2.4; 2.4), cart_velocity, pole_angle(-41.8°, 41.8°), pole_velocity
                act_obs = [o for o in observation]
                act_obs.insert(0, action)
                data.append(np.asarray(act_obs))
                all_data.append(np.asarray(act_obs))
                if done:
                    break
            data_np = np.asarray(data)
            self.save(i, data_np)
        self.save(2, np.asarray(all_data), every=True)
        environment.close()


class RlDataSaver:
    def __init__(self, error_rate):
        self.error_rate = error_rate
        self.save_directory = Path(f'plots/RL_plots/source_files/final{self.error_rate}')
        if not self.save_directory.exists():
            os.makedirs(self.save_directory)

    def save_rewards(self, file_name, rewards):
        np.save(Path(self.save_directory / file_name), rewards)


class RlData:
    def __init__(self, error_rate, epochs, alpha):
        self.error_rate = error_rate
        self.epochs = epochs
        self.lr = alpha

        self.save_directory = Path(f'plots/RL_plots/source_files/final{self.error_rate}')
        if not self.save_directory.exists():
            raise FileNotFoundError('No Reinforcement Learning Data Created. Please Run learn_all with True')

    def set_error_rate(self, new_error_rate):
        self.error_rate = new_error_rate
        self.save_directory = Path(f'plots/RL_plots/source_files/final{self.error_rate}')

    def load_loss_predictions(self, queue_sizes):
        loss_predictions = []
        for queue_size in queue_sizes:
            current_loss_pred = np.load(Path(f'{self.save_directory}') /
                                        f'rewards_LR{self.lr}_epochs{self.epochs}_LossyTrue_prediction_'
                                        f'errorRate{self.error_rate}_completeloss_{queue_size}.npy')
            loss_predictions.append(current_loss_pred)
        return loss_predictions

    def load_complete_data(self):
        return np.load(Path(f'{self.save_directory}') /
                       f'rewards_LR{self.lr}_epochs{self.epochs}_LossyFalse.npy')

    def load_last_value_arrays(self, queue_sizes):
        last_value_array = []
        for queue_size in queue_sizes:
            current_last_value = self.load_data_with_last_value(queue_size)
            last_value_array.append(current_last_value)
        return last_value_array

    def load_data_with_last_value(self, queue_size='0'):
        return np.load(Path(f'{self.save_directory}') /
                       f'rewards_LR{self.lr}_epochs{self.epochs}_LossyTrue_last_value_'
                       f'errorRate{self.error_rate}_completeloss_{queue_size}.npy')

    def load_data_with_randoms(self):
        try:
            return np.load(Path(f'{self.save_directory}') /
                           f'rewards_LR{self.lr}_epochs{self.epochs}_LossyTrue_random_'
                           f'errorRate{self.error_rate}_completeloss_1.npy')
        except FileNotFoundError:
            return None

    def load_reward_arrays(self, queue_sizes):
        complete_rewards = self.load_complete_data()
        predicted_rewards = self.load_loss_predictions(queue_sizes)
        last_value_lossy_rewards = self.load_last_value_arrays(queue_sizes)
        random_lossy_rewards = self.load_data_with_randoms()
        return complete_rewards, predicted_rewards, last_value_lossy_rewards, random_lossy_rewards

    def load_last_reward_arrays(self, queue_sizes):
        c, p, l, r = self.load_reward_arrays(queue_sizes)
        c_last = c[-1]
        p_last = [pred_value[-1] for pred_value in p]
        l_last = [last_value[-1] for last_value in l]
        r_last = r[-1]
        return c_last, p_last, l_last, r_last


if __name__ == '__main__':
    """ 
    Start this Main Method to create training samples of an environment (e.g. 'CartPole-v0')
    """
    env = gym.make('CartPole-v0')
    saving_destination_directory = Path("stop_if_done")
    data_loader = EnvironmentLoader(saving_destination_directory)
    data_loader.create_data(env, start=0, stop=10_000)
