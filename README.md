# Inbetriebnahme des HCNN
1. Generierung der Trianingsdaten für das HCNN: Ausführen von `data_control.py` erstellt 10 000 Trainingssequenzen
2. Trainieren des HCNN: Ausführen von `training_main.py` trainiert das HCNN mit vorausgewählten performanten Hyperparametern

# Verwendung des HCNN
In `rl_main.py` wird ein q-learning agent für Cart-Pole trainiert. Mit der Wahrscheinlichkeit `self.error_rate` fällt eine Observation des Agenten aus und wird durch eine Vorhesage ersetzt. Die Art der Vorhersagemethode wird durch `self.lossy_type` bestimmt: möglich sind `'prediction'` (HCNN Vorhersage), `'last_value'` (Konstanthalten der letzten vollständigen Observation) und `'random'` (Einsetzen zufälliger Werte aus dem legitimen Wertebereich der Observation).

In der Main-Methode von `rl_main.py` sind weiter Flags setzbar: `learn_all` = Boolean bestimmt, ob der Learner zur Datengenerierung (`True`) oder Datenauswertung (`False`) verwendet wird. 

Im letzteren Fall können über `compare_charts` = Boolean und `compare_relations` = Boolean zwei verschiedene Arten von Plots ausgegeben werden. `compare_charts` erzeugt für die in `self.error_rate` festgelegte error_rate einen Graphen, der die mittlerern Rewards des q-learning Agenten für verschiedene queue sizes und lossy_types (prediction, random, last_value) gegeneinander aufträgt.

`compare_relations` erzeugt einen Plot, der für jede error_rate und über alle queue sizes einen Quotienten aus den besten mittleren Rewards von prediction und last_value erzeugt (effektiv also eine Metrik dafür, wie gut die rewards des Agenten bei Verwendung des HCNN im Vergleich zur Verwendung von last_value sind). Alle diese Quotienten werden in einem Plot ausgegeben und zeigen somit auf einen Blick welche queue size und welche error_rate am Besten performen.

