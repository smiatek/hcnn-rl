from torch import nn
import torch


class HCNN(nn.Module):
    def __init__(self, input_size, hidden_dim, std=None, mean=None, activation=nn.Tanh()):
        super().__init__()
        self.input_size = input_size  # Data Dimension of observation
        if std is None:
            std = torch.zeros(self.input_size)
        if mean is None:
            mean = torch.zeros(self.input_size)
        self.hidden_dim = hidden_dim  # e.g. 50 size of hidden layer (= number of neurons in hidden layer)
        # input with action, output without action
        self.A = torch.nn.Linear(in_features=self.hidden_dim + 1, out_features=self.hidden_dim, bias=False)
        identity = torch.cat(
            (torch.eye(self.input_size), torch.zeros(self.hidden_dim - self.input_size, self.input_size)), dim=0)
        self.register_buffer('Id', identity)  # Register in the Module -> same device, and saving to state_dict
        self.activation_function = activation
        self.register_buffer('std', std)
        self.register_buffer('mean', mean)

        self.get_predicted_sequence = []

        # initial bias
        self.init_state = torch.nn.Parameter(torch.randn(hidden_dim), requires_grad=True)
        torch.nn.init.normal_(self.init_state, std=0.1)  # normal distribution of the init state

    def forward(self, input_seq, state=None):
        """
        Takes an input sequence, feeds it through the network and outputs a prediction for time step sequence+1
        :param input_seq:
        :param state:
        :return:
        """

        # find initial state for first step of sequence
        if state is None:
            state = self.init_state.clone()
            # set top 4 neurons to be input data
            state[:self.input_size] = input_seq[0][1:]

        scaled_prediction = torch.zeros(4).float()
        for step in input_seq:
            action = self.get_action(step, converted=True)
            inp = torch.cat((state, torch.tensor([action])), dim=0).float()  # concatenate last neuron to be action
            # actual forward pass
            network_prediction = self.activation_function(self.A(inp))
            # isolate observation
            observation_pred = network_prediction[:self.input_size]
            scaled_prediction = self.scale_observation(observation_pred)  # scale observation output to fit actual range
            # teacher forcing on output state
            state = state - torch.mm(self.Id, (scaled_prediction - step[1:]).view(self.input_size, 1).float()).view(-1)

        return scaled_prediction, state  # pred_observation: size 4, next_network_state: size 50

    def scale_observation(self, observation):
        scaled_prediction = torch.mul(observation, self.std).float()
        scaled_prediction = torch.add(scaled_prediction, self.mean).float()
        return scaled_prediction

    @staticmethod
    def get_action(data_at_time_step, converted=False):
        action = data_at_time_step[0]
        if converted and action == 0:  # converts action 0 to action -1
            action = torch.tensor(-1)
        return action


class HcnnLSTM(HCNN):
    def __init__(self, input_size, hidden_dim, std=None, mean=None):
        super().__init__(input_size, hidden_dim, std, mean)
        use_bias = True
        self.linear_forget_W = nn.Linear(self.hidden_dim + 1, self.hidden_dim, bias=use_bias)
        self.linear_forget_U = nn.Linear(self.hidden_dim, self.hidden_dim, bias=use_bias)
        self.linear_input_W = nn.Linear(self.hidden_dim + 1, self.hidden_dim, bias=use_bias)
        self.linear_input_U = nn.Linear(self.hidden_dim, self.hidden_dim, bias=use_bias)
        self.linear_output_W = nn.Linear(self.hidden_dim + 1, self.hidden_dim, bias=use_bias)
        self.linear_output_U = nn.Linear(self.hidden_dim, self.hidden_dim, bias=use_bias)
        self.linear_cell_W = nn.Linear(self.hidden_dim + 1, self.hidden_dim, bias=use_bias)
        self.linear_cell_U = nn.Linear(self.hidden_dim, self.hidden_dim, bias=use_bias)

    def forward(self, input_seq, state=None, c=None):
        seq_len, _ = input_seq.shape
        h_seq = torch.zeros(seq_len, self.hidden_dim)
        state = state if (state is not None) else self.init_state.clone()
        c_prev = torch.zeros(self.hidden_dim)

        scaled_prediction = torch.zeros(4).float()
        for t, step in enumerate(input_seq):
            action = self.get_action(step, converted=True)
            inp = torch.cat((state, torch.tensor([action])), dim=0).float()  # concatenate last neuron to be action
            forget_gate = torch.sigmoid(self.linear_forget_W(inp).add(self.linear_forget_U(state)))
            input_gate = torch.sigmoid(self.linear_input_W(inp).add(self.linear_input_U(state)))
            output_gate = torch.sigmoid(self.linear_output_W(inp).add(self.linear_output_U(state)))
            c_prev = (torch.mul(forget_gate, c_prev)).add(torch.mul(input_gate, self.activation_function(
                self.linear_cell_W(inp).add(self.linear_cell_U(state)))))
            state = torch.mul(output_gate, self.activation_function(c_prev))
            observation_pred = state[:self.input_size]
            scaled_prediction = self.scale_observation(observation_pred)  # scale observation output to fit actual range
            state = state - torch.mm(self.Id, (scaled_prediction - step[1:]).view(self.input_size, 1).float()).view(-1)
            h_seq[t] = state

        h = state
        c = c_prev
        return scaled_prediction, (h, c)
